package com.devcamp.pizza365.service;

import java.util.ArrayList;


import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Service;

import com.devcamp.pizza365.model.CVoucher;
import com.devcamp.pizza365.responsitory.IVoucherRepository;

@Service
public class CVoucherService {
    @Autowired
	IVoucherRepository pVoucherRepository;
    public ArrayList<CVoucher> getVoucherList() {
        ArrayList<CVoucher> listCVoucher = new ArrayList<>();
        pVoucherRepository.findAll().forEach(listCVoucher::add);
        return listCVoucher;
    }
}