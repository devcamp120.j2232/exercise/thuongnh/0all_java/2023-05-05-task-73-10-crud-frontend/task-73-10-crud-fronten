package com.devcamp.pizza365.responsitory;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.pizza365.model.COrder;

public interface IOrderReponsitory  extends JpaRepository<COrder, Long>{
    
}
