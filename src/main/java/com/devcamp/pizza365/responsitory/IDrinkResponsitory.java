package com.devcamp.pizza365.responsitory;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.pizza365.model.CDrink;

public interface IDrinkResponsitory  extends JpaRepository<CDrink, Long>{
    Optional<CDrink>  findById(long id);    
    
}
