package com.devcamp.pizza365.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.model.CUser;
import com.devcamp.pizza365.responsitory.IUserReponsitory;


@RestController
@CrossOrigin(value = "*", maxAge = -1) // maxAge = -1 không lưu cache vào máy khách
@RequestMapping("/user")
public class CUsersController {

    @Autowired
    IUserReponsitory pCuserRepository;

    // all
    @GetMapping("/all")
    public ResponseEntity<Object> getAllUsers() {
        // ResponseEntity<Object> tạo ra đối tượng responseEntity với kiểu dữ liệu trả
        // về là Object

        List<CUser> userList = new ArrayList<CUser>();
        pCuserRepository.findAll().forEach(userList::add);

        // kiểm tra xem list có rỗng hãy không . nếu rỗng thì báo lỗi ..
        if (!userList.isEmpty()) {
            return new ResponseEntity<>(userList, HttpStatus.OK);

        } else {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

        }

    }

    // thông tin user theo id 
    @GetMapping("/detail/{idUser}")
    public ResponseEntity<Object> getUserById(@PathVariable  long idUser) {
        Optional<CUser> userFouned = pCuserRepository.findById(idUser);
        // Optional để giải quyết các vấn đề về truy cập vào các đối tượng có thể null.
        // Nó đóng vai trò là một bao bọc an toàn cho các giá trị có thể không tồn tại,
        // giúp tránh những lỗi NullPointerException khi truy cập vào đối tượng null.
        if (userFouned.isPresent()) {
            return new ResponseEntity<>(userFouned, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // 7 tạo mới user
    @PostMapping("/create")
    public ResponseEntity<Object> createUser(@RequestBody CUser userFormClient) {

        try {
            // khởi tạo 1 đối tượng để lưu kết người dùng cần tọa
            CUser _user = new CUser(userFormClient.getFullName(), userFormClient.getPhone(),
                    userFormClient.getAddress());
            Date _now = new Date(); // lấy ngày tạo từ hệ thông
            _user.setNgayTao(_now);
            _user.setNgayCapNhap(null);
            pCuserRepository.save(_user);
            return new ResponseEntity<Object>(_user, HttpStatus.OK);

        } catch (Exception e) {
            // TODO: handle exception
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to create specified user" + e.getCause().getCause().getMessage());

        }
    }

    // update 
    @PutMapping("/update/{idUser}")
    public ResponseEntity<Object> updateUser(@PathVariable(name = "idUser") Long paramId, @RequestBody CUser userUpdate) {

        // tìm user theo id trên data base
        Optional<CUser> _userData = pCuserRepository.findById(paramId);
        // kiểm tra có null hay k ,, true là khác null
        if (_userData.isPresent()) {
            // get(); là của Optional để lấy giá tri của đối tượng
            CUser _user = _userData.get(); // lấy giá trị của _userData gán cho _user
            _user.setFullName(userUpdate.getFullName());
            _user.setPhone(userUpdate.getPhone());
            _user.setAddress(userUpdate.getAddress());
            _user.setNgayCapNhap(new Date());

            try {
                return ResponseEntity.ok(pCuserRepository.save(_user));

            } catch (Exception e) {
                // TODO: handle exception
                return ResponseEntity.unprocessableEntity()
                        .body("can not execute operation of this Entity" + e.getCause().getCause().getMessage());
                // không thể thực thi hoạt động của Thực thể này
            }
        } else {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

        }

    }

    
    // delete user //thì xóa luôn all orders thuộc user đó
    @DeleteMapping("/delete/{idUser}")
    public ResponseEntity<Object> deleteUser(@PathVariable Long idUser) {
        Optional<CUser> _userData = pCuserRepository.findById(idUser);
        // nếu khác null
        if (_userData.isPresent()) {
            try {
                pCuserRepository.deleteById(idUser);
                return new ResponseEntity<Object>("đã xóa người dùng có id là  " + idUser, HttpStatus.OK);
            } catch (Exception e) {
                // TODO: handle exception
                return ResponseEntity.unprocessableEntity()
                        .body("can not execute operation of this Entity" + e.getCause().getCause().getMessage());
                // không thể thực thi hoạt động của Thực thể này
            }

        } else {
            return new ResponseEntity<Object>("không tìm thấy user", HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

}
