package com.devcamp.pizza365.controller;

import com.devcamp.pizza365.model.CCountry;
import com.devcamp.pizza365.responsitory.CountryRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@CrossOrigin
public class CountryController {
	@Autowired
	private CountryRepository countryRepository;

	// list countries cập nhập mơi
	@GetMapping("/country/all")
	public ResponseEntity<Object> getAllCountry() {
		try {
			List<CCountry> newCountry = new ArrayList<CCountry>();
			countryRepository.findAll().forEach(newCountry::add);
			return new ResponseEntity<>(newCountry, HttpStatus.OK);

		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// country mẫu
	// {
	// "countryCode": "VN",
	// "countryName": "viet nam",
	// "regions": []
	// }
	@PostMapping("/country/create")
	public ResponseEntity<Object> createCountry(@RequestBody CCountry cCountry) {
		try {
			CCountry newCountry = new CCountry();
			newCountry.setCountryName(cCountry.getCountryName());
			newCountry.setCountryCode(cCountry.getCountryCode());
			newCountry.setRegions(cCountry.getRegions());
			CCountry savedRole = countryRepository.save(newCountry);
			// CCountry savedRole = countryRepository.save(cCountry);
			return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity()
					.body("Failed to Create specified Voucher: " + e.getCause().getCause().getMessage());
		}
	}

	 //GET dữ liệu theo id detail 
	 @GetMapping("/country/{countryId}")
	 public ResponseEntity<Object> getCMenuById(@PathVariable("countryId") Long id) {
		 try {
			 Optional<CCountry> voucherData = countryRepository.findById(id);
			 //Todo: viết code lấy voucher theo id tại đây
			 if (voucherData.isPresent()) {
				 return new ResponseEntity<>(voucherData.get(), HttpStatus.OK);
			 } else {
				 return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			 }
		 } catch (Exception e) {
			 System.out.println(e);
			 return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		 }
	 }

	@PutMapping("/country/update/{idCountry}")
	public ResponseEntity<Object> updateCountry(@PathVariable("idCountry") Long idCountry, @RequestBody CCountry cCountry) {
		Optional<CCountry> countryData = countryRepository.findById(idCountry);
		if (countryData.isPresent()) {
			CCountry newCountry = countryData.get();
			newCountry.setCountryName(cCountry.getCountryName());
			newCountry.setCountryCode(cCountry.getCountryCode());
			newCountry.setRegions(cCountry.getRegions());
			CCountry savedCountry = countryRepository.save(newCountry);
			return new ResponseEntity<>(savedCountry, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("/country/delete/{idCountry}")
	public ResponseEntity<Object> deleteCountryById(@PathVariable Long idCountry) {
		try {
			countryRepository.deleteById(idCountry);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/country/count")
	public long countCountry() {
		return countryRepository.count();
	}

	/**
	 * Viết method kiểm tra country có trong CSDL hay không: checkCountryById() sử
	 * dụng hàm existsById() của CountryRepository
	 * 
	 * @param id
	 * @return
	 */
	@GetMapping("/country/check/{idCountry}")
	public boolean checkCountryById(@PathVariable Long idCountry) {
		return countryRepository.existsById(idCountry);
	}

	/**
	 * Tìm country có chứa giá trị trong country code
	 * 
	 * @param code
	 * @return
	 */
	@GetMapping("/country/containing-code/{code}")
	public List<CCountry> getCountryByContainingCode(@PathVariable String code) {
		return countryRepository.findByCountryCodeContaining(code);
	}

	@GetMapping("/country/details/{idCountry}")
	public CCountry getCountryById(@PathVariable Long idCountry) {
		if (countryRepository.findById(idCountry).isPresent())
			return countryRepository.findById(idCountry).get();
		else
			return null;
	}

	@GetMapping("/country1")
	public CCountry getCountryByCountryCode(@RequestParam(value = "countryCode") String countryCode) {
		return countryRepository.findByCountryCode(countryCode);
	}

	@GetMapping("/country2")
	public List<CCountry> getCountryByCountryName(@RequestParam(value = "countryName") String countryName) {
		return countryRepository.findByCountryName(countryName);
	}

	@GetMapping("/country3")
	public CCountry getCountryByRegionCode(@RequestParam(value = "regionCode") String regionCode) {
		return countryRepository.findByRegionsRegionCode(regionCode);
	}

	@GetMapping("/country4")
	public List<CCountry> getCountryByRegionName(@RequestParam(value = "regionName") String regionName) {
		return countryRepository.findByRegionsRegionName(regionName);
	}

	// API để phân trang
	@GetMapping("/country5")
	public ResponseEntity<List<CCountry>> getCountryByPage(
			@RequestParam(value = "page", defaultValue = "1") String page,
			@RequestParam(value = "size", defaultValue = "5") String size) {
		try {
			Pageable pageWithFiveElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));
			System.out.println(pageWithFiveElements);
			List<CCountry> list = new ArrayList<CCountry>();
			countryRepository.findAll(pageWithFiveElements).forEach(list::add);
			return new ResponseEntity<>(list, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

}
